<?php

/**
 * 
 */
class Mlistblog extends CI_Model
{
	public function getListBlog()
	{
		$cate = $this->db->get('category')->result_array();
		foreach ($cate as $k => $v) {
			$this->db->where('category_id', $v['id']);
			$this->db->select('posts.*, users.name as poster');
			$this->db->join('users', 'users.id = posts.user_id');
			$cate[$k]['posts'] = $this->db->get('posts')->result_array();
		}
		return $cate;
	}

	public function deletePost($id)
	{
		$this->db->trans_start();
		$this->db->where('post_id', $id);
		$this->db->or_where('post_tag', $id);
		$this->db->delete('posts_tag');
		$this->db->where('id', $id);
		$this->db->delete('posts');
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
}