<?php
/**
 * Created by PhpStorm.
 * User: Nguyễn Duy Thành
 * Date: 03/09/2019
 * Time: 04:11 CH
 */

class Mlogin extends MY_Model
{
    public function checkAccount($email, $id)
    {
        $this->db->where('email', $email);
        $this->db->where('id', $id);
        return $this->db->get('users')->row_array();
    }

	public function getAccount($email, $pass)
	{
		$this->db->like('email', $email);
		$this->db->where('password', sha1($pass));
		return $this->db->get('users')->row_array();
	}
}