<?php

/**
 * 
 */
class Maddblog extends CI_Model
{
	public function getCategory()
	{
		return $this->db->get('category')->result_array();
	}

	public function getTags($id='')
	{
		if (!empty($id)) {
			$this->db->where('id != ', $id);
		}
		$this->db->select('id, name');
		return $this->db->get('posts')->result_array();
	}

	public function savePost($data)
	{
		$this->db->insert('posts', $data);
		return $this->db->insert_id();
	}

	public function saveTags($idPost, $idTags)
	{
		if (!empty($idTags)) {
			$rs = 0;
			$this->db->trans_start();
			$this->db->where('post_id', $idPost);
			$this->db->delete('posts_tag');
			foreach ($idTags as $k => $v) {
				$this->db->insert('posts_tag',['post_id' => $idPost, 'post_tag' => $v]);
				$rs += $this->db->affected_rows();
			}
			$this->db->trans_complete();
			return $rs;
		}
		else{
			return true;
		}
	}

	public function getPostEdit($id)
	{
		$this->db->where('id', $id);
		$post = $this->db->get('posts')->row_array();
		$this->db->where('post_id', $id);
		$tags = $this->db->get('posts_tag')->result_array();
		$arr_tag = [];
		foreach ($tags as $key => $value) {
			$arr_tag[] = $value['post_tag'];
		}
		$post['tags'] = $arr_tag;
		return $post;
	}

	public function editPost($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('posts', $data);
		return $this->db->affected_rows();
	}
}