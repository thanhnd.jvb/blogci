<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']		= 'Clogin';
$route['404_override']				= '';
$route['translate_uri_dashes']		= FALSE;
$route['nojs.html']					= 'Cnojs';

//Hệ thống
$route['login']						= 'Clogin';
$route['logout']					= 'Clogin/logout';
$route['register']					= 'hethong/Cdangky';
$route['welcome']					= 'Cwelcome';
$route['addblog']					= 'Caddblog';
$route['listblog']					= 'Clistblog';
