<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database','Core_smarty','session');

$autoload['drivers'] = array();

$autoload['helper'] = array('url','my');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();
