<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Admin blog CI</title>

    <link rel="icon" href="{$url}assets/plugins/img/logo.png">

    <link href="{$url}assets/plugins/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$url}assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{$url}assets/plugins/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <link href="{$url}assets/plugins/css/animate.css" rel="stylesheet">
    <link href="{$url}assets/plugins/css/style.css" rel="stylesheet">
    <link href="{$url}assets/plugins/css/your_style.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="wrapper animated fadeInRight">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-6 col-xs-12 col-sm-12 col-md-offset-3">
                        <form action="" method="post" class="form-horizontal">
                            <input type="hidden" name="{$csrf['name']}" value="{$csrf['hash']}" />
                            <div class="para-head text-center m-b">
                                <h2>Đăng nhập</h2>
                                <hr>
                            </div>
                            <div class="para-body m-t-lg">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tài khoản</label>
                                    <div class="col-md-9">
                                        <input type="text" name="email" value="{$account}" class="form-control" placeholder="Tài khoản" required autofocus="true">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Mật khẩu</label>
                                    <div class="col-md-9">
                                        <input type="password" name="pass" class="form-control" placeholder="******" required>
                                    </div>
                                </div>
                                <div class="col-md-12 height-10 text-center m-t-sm hidden-xs" id="errorPlace">

                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 m-t" style="padding-right: 0;">
                                    <div class="pull-right">
                                        <button type="submit" name="login" value="login" class="btn btn-primary">Đăng nhập</button>
                                    </div>
                                    <div class="pull-left m-t-sm">
                                        <!-- Chưa có tài khoản? <a href="{$url}register">Đăng ký</a> -->
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Mainly scripts -->
    <script src="assets/plugins/js/jquery-2.1.1.js"></script>
    <script src="assets/plugins/js/bootstrap.min.js"></script>
    <script src="assets/plugins/js/plugins/toastr/toastr.min.js"></script>
    <script type="text/javascript">
        toastr.options = {
            top: 500,
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 5000
        };
    </script>
    {if !empty($message)}
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                toastr.{$message.type}('{$message.message}');
            }, 200);
        });
    </script>
    {/if}
</body>
</html>
