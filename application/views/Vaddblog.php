<script src="{$url}assets/ckeditor/ckeditor.js"></script>
<script src="{$url}assets/ckfinder/ckfinder.js"></script>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Blog</h2>
		<ol class="breadcrumb">
			<li class="active">Thêm bài viết</li>
		</ol>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Thêm bài viết</h5>
				</div>
				<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="{$csrf['name']}" value="{$csrf['hash']}" />
                    <div class="ibox-content">
                    	<div class="row">
	                    	<div class="col-md-7 col-xs-7">
	                    		<div class="form-group">
	                    			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tieude">Tiêu đề</label>
	                    			<div class="col-md-9 col-sm-9 col-xs-12">
	                    				<input type="text" name="name" id="tieude" class="form-control" value="{(isset($postedit.name)) ? $postedit.name : NULL}" required>
	                    			</div>
	                    		</div>

	                    		<div class="form-group">
	                    			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="mota">Mô tả</label>
	                    			<div class="col-md-9 col-sm-9 col-xs-12">
	                    				<textarea name="description" id="mota" class="form-control" rows="6" required>{(isset($postedit.description)) ? $postedit.description : NULL}</textarea>
	                    			</div>
	                    		</div>

	                    		<div class="form-group">
	                    			<label class="control-label col-md-3 col-sm-3 col-xs-12">Danh mục</label>
	                    			<div class="col-md-9 col-sm-9 col-xs-12">
	                    				<select class="form-control" name="category" required>
	                    					<option value=""></option>
	                    					{foreach $category as $key => $value}
	                    						<option value="{$value.id}" {(isset($postedit.description) && $postedit.category_id == $value.id) ? 'selected' : ''}>
	                    							{$value.name}
	                    						</option>
	                    					{/foreach}
	                    				</select>
	                    			</div>
	                    		</div>
	                    	</div>
	                    	<div class="col-md-5 col-xs-5">
	                    		<div class="form-group">
	                    			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tags">Tags</label>
	                    			<div class="col-md-9 col-sm-9 col-xs-12">
	                    				<select class="form-control select2" name="tags[]" multiple="multiple">
	                    					<option value=""></option>
	                    					{foreach $tags as $key => $value}
	                    						<option value="{$value.id}" {(isset($postedit.tags) && in_array($value.id, $postedit.tags)) ? 'selected' : ''}>
	                    							{$value.name}
	                    						</option>
	                    					{/foreach}
	                    				</select>
	                    			</div>

	                    		</div>

	                    		<div class="form-group">
	                    			<label class="control-label col-md-3 col-sm-3" for="uploadanh">Ảnh đại diện</label>
	                    			<div class="col-md-9 col-sm-9 col-xs-12">
	                    				<input type="file" id="uploadanh" name="image" accept=".png, .jpg">
	                    			</div>
	                    			<br>
	                    			<br>
	                    			<img width="100%" src="{if (isset($postedit.image) && !empty($postedit.image))}{$url}{$postedit.image}{/if}">
	                    		</div>
	                    	</div>

	                    	<div class="col-md-12">
	                    		<div class="form-group">
	                    			<label class="control-label col-md-12 col-sm-12 col-xs-12 left" for="detail" style="text-align: left;">Nội dung</label>
	                    			<div class="col-md-12 col-sm-12 col-xs-12">
	                    				<textarea name="detail" rows="8" id="detail" required="required" class="form-control">{(isset($postedit.detail)) ? $postedit.detail : NULL}</textarea>
	                    			</div>
	                    		</div>
	                    		<div class="pull-right">
                    				<button style="margin-left: 150px;" type="submit" value="{(isset($postedit.id)) ? $postedit.id : 'save'}" class="btn btn-success" name="{(isset($postedit.id)) ? 'edit' : 'save'}">Lưu</button>
                				</div>
	                    	</div>
                    	</div>
                    </div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		CKEDITOR.replace('detail');
        $(".select2_demo_2").select2();
	});
</script>