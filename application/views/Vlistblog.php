<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Blog</h2>
		<ol class="breadcrumb">
			<li class="active">Danh sách bài viết</li>
		</ol>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Danh sách bài viết</h5>
				</div>
				<form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="{$csrf['name']}" value="{$csrf['hash']}" />
                    <div class="ibox-content">
                    	<table class="table table-bordered">
                    		<thead>
                    			<th class="text-center">STT</th>
                    			<th>Tiêu đề</th>
                    			<th>Mô tả</th>
                    			<th class="text-center">Thời gian</th>
                    			<th class="text-center">Người đăng</th>
                    			<th class="text-center">Tác vụ</th>
                    		</thead>
                    		<tbody>
                    			{$stt = 1}
                    			{foreach $listBlog as $key => $val}
	                    			<tr>
	                    				<td colspan="6">Category {$key+1}: <strong>{$val.name}</strong></td>
	                    			</tr>
	                    			{if !empty($val.posts)}
	                    				{foreach $val.posts as $k => $v}
	                    					<tr>
			                    				<td class="text-center">{$stt++}</td>
			                    				<td>{$v.name}</td>
			                    				<td>{$v.description}</td>
			                    				<td class="text-center">{date('d/m/Y H:i:s', $v.timestamp)}</td>
			                    				<td class="text-center">{$v.poster}</td>
			                    				<td class="text-center">
			                    					<a href="addblog?id={$v.id}" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></a>
			                    					<button type="submit" class="btn btn-xs btn-danger" value="{$v.id}" name="deletePost" onclick="return confirm('Bạn chắc chắn muốn xoá bài viết này?');"><i class="fa fa-trash-o"></i></button>
			                    				</td>
			                    			</tr>
		                    			{/foreach}
	                    			{else}
	                    				<tr>
	                    					<td colspan="6"><i>Không có bài viết nào</i></td>
	                    				</tr>
	                    			{/if}
                    			{/foreach}
                    		</tbody>
                    	</table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>