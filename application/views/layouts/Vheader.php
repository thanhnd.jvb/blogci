<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <base href="{$url}">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

	<title>Admin blog CI</title>

    <link rel="icon" href="{$url}assets/plugins/img/logo.png">
	<link href="{$url}assets/plugins/css/bootstrap.min.css" rel="stylesheet">
	<link href="{$url}assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="{$url}assets/plugins/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="{$url}assets/plugins/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="{$url}assets/plugins/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="{$url}assets/plugins/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="{$url}assets/plugins/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

	<link href="{$url}assets/plugins/css/animate.css" rel="stylesheet">
	<link href="{$url}assets/plugins/css/style.css" rel="stylesheet">
	<link href="{$url}assets/plugins/css/custom_style.css" rel="stylesheet">

    <script src="assets/plugins/js/jquery-2.1.1.js"></script>
    <script src="assets/plugins/js/bootstrap.min.js"></script>
    <script src="assets/plugins/js/plugins/validate/jquery.validate.min.js"></script>
    <script type="text/javascript">
        var tokenName = '{$csrf["name"]}';
        var tokenValue = '{$csrf["hash"]}';
        $(document).ready(function() {
            var token = {};
            token[tokenName] = tokenValue;
            $.ajaxSetup({
                data: token
            });
        });
    </script>
</head>
<body class="md-skin">
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element">
							<span>
							    {assign var="file_path" value="assets/avatars/`$session.id`.jpg"}
								{if (file_exists($file_path))}
									<img alt="image" class="img-circle" src="assets/avatars/{$session['id']}.jpg?ver={time()}" width="48px" height="48px">
								{else}
									<img alt="image" class="img-circle" src="assets/avatars/default_avatar.png" width="48px" height="48px">
								{/if}
							</span>
							<a href=""> <!--  data-toggle="dropdown" class="dropdown-toggle" -->
								<span class="clear">
									<span class="block m-t-xs">
										<strong class="font-bold">{$session.name}</strong>
									</span>
									<!-- <span class="text-muted text-xs block">Cá nhân <b class="caret"></b></span> -->
								</span>
							</a>
							<!-- <ul class="dropdown-menu animated fadeInRight m-t-xs">
								<li><a href="{$url}changepassword">Đổi mật khẩu</a></li>
								<li><a href="{$url}logout">Đăng xuất</a></li>
							</ul> -->
						</div>
						<div class="logo-element">
						</div>
					</li>
					<li>
                        <a href="{$url}addblog" title="Thêm bài viết">
                            <i class="fa fa-pencil-square-o"></i>
                            <span class="nav-label">Thêm bài viết</span>
                        </a>
                    </li>
                    <li>
                        <a href="{$url}listblog" title="Danh sách bài viết">
                            <i class="fa fa-list-ul"></i>
                            <span class="nav-label">Danh sách bài viết</span>
                        </a>
                    </li>
				</ul>
			</div>
		</nav>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
					<div class="navbar-header">
            			<div class="navbar-minimalize minimalize-styl-2 btn btn-primary">
            				<i class="fa fa-bars"></i>
                            <input type="hidden" id="id" value="{$session['id']}" disabled>
            			</div>
            		</div>
					<ul class="nav navbar-top-links navbar-right">
						<li class="hidden-xs">
							<a class="m-r-sm text-muted welcome-message">{$session.name}</a>
						</li>
						<!-- <li><a href="{$url}changepassword">Đổi mật khẩu</a></li> -->
						<li>
							<a href="{$url}logout">
								<i class="fa fa-sign-out"></i><span class="visible hidden-xs">Đăng xuất</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
