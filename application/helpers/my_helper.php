<?php
/**
 * Created by PhpStorm.
 * User: Nguyễn Duy Thành
 * Date: 01/09/2019
 * Time: 08:49 SA
 */


if(!function_exists('pr'))
{
    /**
     * @param $data
     */
    function pr($data) {
        echo "<pre>";
        print_r ($data);
        echo "</pre>";
        exit();
    }
}

if (!function_exists('getSession')){
    function getSession($name = 'user'){
        $CI =& get_instance();
        return $CI->session->userdata($name);
    }
}

if (!function_exists('getSegment')){
    function getSegment(){
        $CI =& get_instance();
        return $CI->uri->segment(1);
    }
}

if(! function_exists('notification'))
{
    function setMessages($toastType = "", $message = "", $title = "") {
        $CI =& get_instance();
        $dataMessage = array(
            'type' => $toastType,
            'title' => $title,
            'message' => $message
        );
        $CI->session->set_flashdata('notification', $dataMessage);
    }
}


if(!function_exists('notification'))
{
    function getMessages() {
        $CI =& get_instance();
        return $CI->session->flashdata('notification');
    }
}

if (!function_exists('checkPermission')){
    function checkPermission($userLevel, $route){
        $CI =& get_instance();
        $CI->load->model('Mlogin', 'Mlogin');
        return $CI->Mlogin->checkPermission($userLevel, $route);
    }
}

if (!function_exists('seotag')){
    /*
    * $valueReturn return value day or month or year
    * $dateString date with format dd/mm/yyyy
    * default return date with format yyyy-mm-dd
    */
    function dateconvert($valueReturn = '', $dateString = '01/01/1970')
    {
        $dateSplit = explode("/", $dateString);
        $day = $dateSplit[0];
        $month = $dateSplit[1];
        $year = $dateSplit[2];
        switch ($valueReturn) {
            case 'd':
                return $day;
            case 'm':
                return $month;
            case 'y':
                return $year;
            case 'Y':
                return $year;
            default:
                return $year.'-'.$month.'-'.$day;
        }
    }
}

if (!function_exists('seotag')){
    function seotag($str)
    {
        $str = trim($str);
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);    
        $str = html_entity_decode ($str);
        $str = str_replace(array(' ','_'), '-', $str); 
        $str = html_entity_decode ($str);
        $str = str_replace("ç","c",$str);
        $str = str_replace("Ç","C",$str);
        $str = str_replace(" / ","-",$str);
        $str = str_replace("/","-",$str);
        $str = str_replace(" - ","-",$str);
        // $str = str_replace("_","-",$str);
        $str = str_replace(" ","_",$str);
        $str = str_replace("ß", "ss", $str);
        $str = str_replace("&", "", $str);
        $str = str_replace("%", "percent", $str);
        $str = str_replace("----","-",$str);
        $str = str_replace("---","-",$str);
        $str = str_replace("--","-",$str);
        $str = str_replace(",","",$str);
        $str = str_replace("(","",$str);
        $str = str_replace(")","",$str);
        // In hoa
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        $str = strtolower($str);

        return $str; // Trả về chuỗi đã chuyển
    } // End seotag
}

function adddotstring($strNum) {
   $len = strlen($strNum);
   $counter = 3;
   $result = "";
   while ($len - $counter >= 0)
   {
       $con = substr($strNum, $len - $counter , 3);
       $result = ','.$con.$result;
       $counter+= 3;
   }
   $con = substr($strNum, 0 , 3 - ($counter - $len) );
   $result = $con.$result;
   if(substr($result,0,1)==','){
       $result=substr($result,1,$len+1);
   }
   return $result;
    //return $strNum;
}

function numberToRoman($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}

#get user ip
function get_client_ip_env() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}


function unique_filename_upload($filename)
{
    return date('d-m-Y', time()) . '__' . date('H-i-s', time()) . '__' . $filename;
}

function getExension($ten)
{
    $st='';

    for($i=strlen($ten)-1; $i>1 ; $i--){
        $st=$st.$ten[$i];

        if($ten[$i]=='.'){
            break;
        }
    }

    $st=strrev($st);
    return $st;
}

function getMangFile($mang,$name)
{
    $s='';
    $mangfile = array();
    for ($j=2; $j < count($mang); $j++) {
        $ten = $mang[$j];
        for($i=strlen($ten)-1; $i>0 ; $i--){
            $s=$s.$ten[$i];

            if($ten[$i]=='.'){
                break;
            }
        }
        $namefile = substr($ten,0,$i);
        $mangfile[] = $namefile;
    }
    return $mangfile;
}
