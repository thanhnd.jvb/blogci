<?php

/**
 * 
 */
class Clistblog extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mlistblog', 'Mlistblog');
		$this->Mlistblog = new Mlistblog();
	}

	public function index()
	{
		if ($this->input->post('deletePost')) {
			$this->deletePost();
		}
		$data = array(
			'listBlog' 	=> $this->Mlistblog->getListBlog()
		);
		$temp['data'] = $data;
		$temp['template'] = 'Vlistblog';
        $this->load->view('layouts/Vlayout', $temp);
	}

	public function deletePost()
	{
		$id = $this->input->post('deletePost');

		$rs = $this->Mlistblog->deletePost($id);
        if($rs){
            setMessages('success','Xoá bài viết thành công');
        }
        else {
            setMessages('error','Xoá bài viết thất bại');
        }
        return redirect('listblog');
	}
}