<?php

/**
 * 
 */
class Caddblog extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Maddblog', 'Maddblog');
		$this->Maddblog = new Maddblog();
	}

	public function index()
	{
		$id = $this->input->get('id');
		if ($this->input->post('save')) {
			$this->savePost();
		}
		if ($this->input->post('edit')) {
			$this->savePost($id);
		}
		$data = array(
			'category' 	=> $this->Maddblog->getCategory(),
			'tags'	 	=> $this->Maddblog->getTags($id),
			'postedit'	=> $this->Maddblog->getPostEdit($id)
		);
		$temp['data'] = $data;
		$temp['template'] = 'Vaddblog';
        $this->load->view('layouts/Vlayout', $temp);
	}

	public function savePost($id)
	{
		if (!empty($id)) {
			$postId = $this->input->post('edit');
			if ($postId != $id) {
	            setMessages('error','Có lỗi xảy ra!');
	        	header("Refresh:0");
	        	exit();
			}
		}

        // insert db
        if(!$_FILES['image']['name']){
            $filename = '';
        }
        else {
			$this->load->library('upload');
	        $name 	  	= time().rand(100,999);
			$ext        = getExension($_FILES['image']['name']);

	        $filename   = $name.$ext;

	        $config['upload_path']          = './assets/plugins/img/';
	        $config['allowed_types']        = 'png|jpg';
	        $config['file_name']        	= $filename;
	        $config['overwrite']        	= true;
	        $this->upload->initialize($config);

	        if (!$this->upload->do_upload('image'))
	        {
	        	$filename = '';
	            $data['error'] =  $this->upload->display_errors();
	        }
	        else
	        {
	            $data['upload_data'] = $this->upload->data();
	        }
	    }

        $name         	= $this->input->post('name');
        $description    = $this->input->post('description');
        $category       = $this->input->post('category');
        $tags         	= $this->input->post('tags');
        $image       	= 'assets/plugins/img/'.$filename;
        $detail        	= $this->input->post('detail');

        $session        = getSession();

        $tags            = $this->input->post('tags');

        $data = array(
        	'name' 			=> $name,
        	'description' 	=> $description,
        	'detail' 		=> $detail,
        	'timestamp' 	=> time(),
        	'category_id' 	=> $category,
        	'user_id' 		=> $session['id'],
        );

        if (!empty($filename)) {
        	$data['image'] = $image;
        }

        if (!empty($id)) {
        	$this->Maddblog->editPost($id, $data);

        	$this->Maddblog->saveTags($id, $tags);
            setMessages('success','Cập nhật bài viết thành công');
        }
        else{
	        $ins_id = $this->Maddblog->savePost($data);

	        if($ins_id){
	        	$this->Maddblog->saveTags($ins_id, $tags);
	            setMessages('success','Thêm bài viết thành công');
	        }
	        else {
	            setMessages('error','Thêm bài viết thất bại');
	        }
	    }
        return redirect('listblog');
	}
}