<?php
/**
 * Created by PhpStorm.
 * User: Nguyễn Duy Thành
 * Date: 03/09/2019
 * Time: 03:57 CH
 */

class Clogin extends CI_Controller
{
	public $Mlogin;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mlogin', 'Mlogin');
		$this->Mlogin = new Mlogin();
	}

	public function index()
	{
		$this->checkIssetSession();
		if ($this->input->post('login')) {
			$this->checkLogin();
		}

		$data['account'] = $this->session->flashdata('account');
		$data['url'] = base_url();
		$data['message'] = getMessages();
		$data['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		$this->parser->parse('Vlogin', $data);
	}

	/**
	 * Kiểm tra nếu đã tồn tại session thì cho đưa vào welcome luôn
	 */
	public function checkIssetSession()
	{
		$session = getSession();
		if (isset($session) && !empty($session)) {
			$checkAccount = $this->Mlogin->checkAccount($session['email'], $session['id']);
			$this->session->set_userdata('user', $session);
			return redirect('welcome');
		}
	}

	public function checkLogin()
	{
		$email = trim($this->input->post('email'));
		$pass = $this->input->post('pass');

		$account = $this->Mlogin->getAccount($email, $pass);

		if (!empty($account)) {
			if (strtolower($email)==strtolower($account['email']) && sha1($pass)==$account['password']) {
				$session = array(
					'id'		=> $account['id'],
					'email' 	=> $account['email'],
					'name' 		=> $account['name']
				);
				$this->session->set_userdata('user', $session);
				setMessages('success', 'Đăng nhập thành công');
				return redirect('welcome');
			}
		}
		$this->session->set_flashdata('account', $email);
		setMessages('error', 'Email hoặc mật khẩu không chính xác. Vui lòng thử lại!');
		return redirect(base_url());
	}

	public function logout()
	{
		$this->session->unset_userdata('user');
		$this->session->sess_destroy();
		redirect(base_url());
		exit();
	}
}