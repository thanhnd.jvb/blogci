<?php
/**
 * Created by PhpStorm.
 * User: Nguyễn Duy Thành
 * Date: 04/09/2019
 * Time: 03:18 CH
 */

class Cwelcome extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $temp['template'] = 'Vwelcome';
        $this->load->view('layouts/Vlayout', $temp);
    }
}