<?php
/**
 * Created by PhpStorm.
 * User: Nguyễn Duy Thành
 * Date: 03/09/2019
 * Time: 03:59 CH
 */

class MY_Model extends CI_Model
{
	public function getAll($table)
	{
		return $this->db->get($table)->result_array();
	}

	public function getAllOrder($table, $col, $order)
	{
		$this->db->order_by($col, $order);
		return $this->db->get($table)->result_array();
	}

	public function getWhere($table, $colWhere, $valueCompare)
	{
		$this->db->where($colWhere, $valueCompare);
		return $this->db->get($table)->result_array();
	}

    public function getWherePrimary($table, $colWhere, $valueCompare)
    {
        $this->db->where($colWhere, $valueCompare);
        return $this->db->get($table)->row_array();
    }
}
